��    )     d  �  �"      8.  @  9.     z/     �/  <   �/     �/     �/     �/     �/  o   �/  o   c0  ;   �0     1     1     1     1     1     %1     A1     J1     W1     `1     d1     m1  
   y1  	   �1     �1     �1     �1     �1  	   �1     �1     �1     �1  	   �1     �1     �1     2  	   &2     02  �   72  �   �2  �   �3     J4     V4     i4     u4     z4  
   �4  X   �4     �4     �4     �4     5     5     25     D5  B   S5  w   �5  P  6     _7     e7     j7  	   o7     y7  
   �7     �7     �7     �7     �7     �7     �7  0   �7  7   �7     %8     *8     <8     A8     T8     \8     k8     �8     �8  !   �8  &   �8     �8     9     9     9     %9     ,9     39     ;9  
   O9  
   Z9     e9     u9     }9     �9     �9     �9     �9  !   �9     �9     �9     :     :     :     ":     *:     2:     ::     B:     F:  	   K:     U:     ^:     l:     |:     �:     �:     �:     �:     �:     �:  p   �:     Z;     n;     ~;     �;     �;     �;     �;     �;     �;     �;     �;     �;     <     <     :<  $   J<  
   o<     z<     �<     �<     �<     �<     �<     �<     �<     �<     �<  
   �<     =     =     =     =     &=  
   3=     >=     M=     i=     q=     x=     �=  &   �=  "   �=     �=     �=     >     >     >     )>     0>  +   >>     j>     �>     �>  "   �>     �>     �>     �>     ?  
   ?     ?     )?     0?     B?     R?     W?     `?     u?     �?     �?     �?     �?     �?     �?     @     @  	   @     &@     -@     >@     R@     c@     t@     �@     �@     �@     �@  '   �@     �@     A     A     &A     ,A     1A     6A  *   HA  �   sA  �   B     �B     �B     �B     �B     �B     �B     �B     �B     �B  
   �B     �B     C  *    C  r   KC     �C     �C     �C     �C     D     D     +D  )   ?D     iD     wD  !   �D  7   �D     �D     �D     E     E     E     (E  '   DE     lE  �   xE     7F     >F     FF     NF     VF     kF     �F     �F     �F     �F     �F     �F  
   �F  H   �F  $   G     5G     >G     YG     `G     mG     rG     zG     �G     �G     �G     �G  )   �G     �G     H     
H  
   H     $H     3H     KH     ZH     iH     nH     zH     �H     �H     �H     �H     �H     �H     �H     �H     �H     I     I     I     I     +I  	   ;I     EI  	   QI     [I     iI  	   pI     zI     �I     �I     �I     �I     �I     �I     �I  
   �I     �I     �I     J     
J     J  	   +J     5J     8J     AJ     JJ     \J     {J     �J     �J  "   �J     �J     �J  	   �J     	K  
   K      K     &K     /K     8K     >K     DK  	   IK  �   SK  (   �K  1   L     9L  %   YL     L     �L     �L     �L      �L     M  $   *M     OM     TM     ]M     {M     �M     �M  	   �M     �M  	   �M     �M     �M     �M     �M     �M     N  	   N     N     .N     6N  
   ;N     FN  ,   JN  $   wN     �N     �N     �N     �N  	   �N     �N     �N     �N     O     O  
   .O     9O     OO     VO     cO     qO     ~O     �O     �O     �O     �O     �O     �O  
   �O     �O     �O     �O  
   �O     P     P  	   /P     9P  
   MP  
   XP  	   cP     mP     �P     �P     �P  
   �P     �P     �P     �P     �P     �P     Q     Q  
   !Q     ,Q  0   3Q  Z   dQ  J   �Q  >   
R  S   IR  M   �R     �R  g   
S  <   rS     �S     �S     �S      �S     T     T     T     %T  	   +T     5T     BT     VT     iT     |T  
   �T     �T  	   �T     �T     �T     �T     �T  
   �T     �T  >   U  
   QU     \U     `U     nU     uU     ~U  )   �U  M   �U  4   V  5   7V  \   mV  _   �V  '   *W  W   RW  �   �W  p   8X  	   �X     �X     �X     �X     �X  	   �X     �X     �X     �X     Y     Y     -Y  %   JY     pY     �Y     �Y  &   �Y  
   �Y  
   �Y     �Y     Z     %Z     *Z  	   8Z     BZ     KZ     TZ     dZ     kZ     oZ     wZ     �Z     �Z     �Z     �Z     �Z     �Z     �Z  K    [  D   L[  V   �[  #   �[  c   \  (   p\     �\     �\     �\     �\     �\  $   �\     �\     �\     �\     �\     ]     ]     ]     ']     >]     U]     Z]  	   b]     l]     q]     �]     �]     �]     �]  �  �]  �  m_  #   Qa     ua  B   wa  ,   �a     �a     �a     �a  �   �a  �   ub  V   5c     �c     �c     �c     �c     �c  2   �c  
   �c     �c     �c     d     d     :d     Td  #   rd  
   �d  %   �d  
   �d     �d     �d     �d     	e     e  	   0e     :e  .   Ge     ve     �e  
   �e  �   �e  �   �f  �   �g     [h  (   wh     �h  
   �h     �h     �h  �   �h     mi     �i  %   �i     �i  +   �i     �i     	j  �   j  �   �j  A  lk     �m  
   �m     �m     �m     �m     �m     n     'n     ;n     On     cn     wn  S   �n  d   �n     =o     Po     ho  ,   wo  
   �o     �o  6   �o  /   �o  1   .p  <   `p  #   �p     �p     �p     �p     q     !q  
   .q     9q     Nq     mq     �q     �q     �q     �q     �q  1   �q  '   )r  ,   Qr  =   ~r  '   �r     �r     s     s     -s     As     Us     is     }s     �s     �s     �s     �s  	   �s     �s     �s     t     t     %t     3t     It     Yt  �   ot  !   0u     Ru     du     vu     �u     �u     �u     �u     �u     �u     �u     �u     v  .   0v     _v  <   wv     �v     �v     �v     �v  !   �v  (   w     =w     Vw     iw     rw  #   �w     �w     �w     �w     �w  5   �w  '    x     Hx     dx  ,   ~x     �x     �x     �x  D   �x  8   6y  ;   oy     �y     �y     �y     �y  !   z     (z     7z  B   Rz  %   �z  &   �z  "   �z  3   {  &   9{     `{     i{     �{     �{  $   �{     �{  '   �{     |     |     (|  4   4|     i|  !   ~|     �|  #   �|     �|  8   �|  4   &}     [}     y}     �}     �}  .   �}  3   �}  /   ~  *   G~  .   r~  *   �~     �~  ,   �~  (     1   5      g     �     �     �     �     �     �  5   �  ,  (�    U�     t�  !   y�     ��     ��     ��     Ƃ     ʂ  -   ΂     ��     ��  &   �  %   A�  A   g�  �   ��     G�     T�     o�  #   ��     ��     ̄     �  N   �     U�  ,   r�  *   ��  P   ʅ     �     5�     U�     j�     y�  '   ��  G   ��     �  �   �     �     #�     0�     G�  #   T�  '   x�     ��     ��     ��  #   ʈ     �     �  
   �  �   (�  ?   ��     �  "   �     +�     B�     \�     i�     q�     y�     ��     ��  *   ��  ?   �  0   *�     [�     c�  
   v�     ��     ��  -   ��     ދ  
   ��     �  4    �  3   U�  
   ��     ��  
   ��     ��     Ì     ܌     �     ��     �  
   (�     3�     A�  $   Q�     v�     ��     ��     ��     ֍     �     ��  )   �  )   @�     j�  2   q�     ��     ��     ͎     �     �     �  
   �  !   *�     L�     f�     y�     ~�     ��     ��  2   ��     �     �  #   �  6   4�     k�     ��     ��  
   ��  #   ��     �     �     ��     �     �     �  	   "�  �   ,�  .   �  "   �  .   <�  *   k�  &   ��     ��  5   ג      �  ;   .�  $   j�  G   ��     ד     ��     ��     �      �     9�     M�     g�     ��     ��     ��     є     ޔ  (   �     �      �     1�     F�  
   U�     `�     v�  b   z�  B   ݕ  9    �     Z�     i�     o�     �     ��  #   ��     Ζ     �     ��  
   �  )   �     G�  '   V�  %   ~�  '   ��  #   ̗     �     �     �     �     5�     J�     ]�  
   w�  
   ��     ��     ��     ��  %   ʘ     �     ��     �     )�     A�  !   T�     v�     ��     ��     ��     ؙ  
   �     ��  '   �  !   <�  
   ^�     i�  %   ��     ��  M   ��  �   ��  �   ��  U   �  g   q�  f   ٜ  3   @�  o   t�  y   �  2   ^�  ,   ��  4   ��  1   �     %�     :�     K�  
   h�     s�     ��  $   ��  4   ��  $   �  *   �     C�  
   U�     `�     z�  %   ��  %   ��     �     �  *   ��  M   '�     u�     ��     ��     ��     ʡ     ס  N   �  f   B�  X   ��  W   �  �   Z�  �   �  F   ��  �   Ϥ  #  ]�  �   ��     D�     Z�      k�     ��     ��     ��     ��  
   ȧ  6   ӧ     
�  5   (�  5   ^�  F   ��  #   ۨ  !   ��     !�  H   @�     ��     ��  D   ǩ  ?   �  
   L�  )   W�     ��  
   ��  
   ��  )   ��     �     �     �  )   	�  (   3�  6   \�  0   ��  
   ī  %   ϫ     ��     �  T   �  z   b�  x   ݬ  2   V�  �   ��  E   ;�     ��     ��     ��     ��     ��  =   ��     �     ��      �     �     �     .�     >�  0   W�  *   ��     ��     Ư  	   ί     د  +   ݯ  
   	�  )   �     >�     D�        o  e  5  %       �   �   �           8  �  �  �         �      �          ;         �   �   �  �       p        k   �         �      x  �  +       	   �               C  '              �  R     �      �   �           6               "   �  �       �   \   �   �   �       �   �                  �   k                �      �  �   (  a  	  �            �    �                 �  �   �      b  �    �  �             
  �      �   U         �      W   �                �      �   �       y   �   g  �           |  �         8   t   �     �   +      �          Z  z  L       %     i  -   �     O  �                �  :   �   \  �   �   '  {  Q  �          �  h  �         �        [   #  �   E  s      �      �   �  �   �      �      Q   m      �   �       {   �         �  �  !      K  A   J  �      �  �     *  t  �          �       S              �  �           �    �  �       _    �               d  I     Z       $  �   ;      j  3  H  �   (  �   v              �            &   4   �  J           �   b   r         �  !   �  u     R      �       =  �   P   #   a   �   �  1  F  g   �                 _   �  �  �               �       �  i           �      �  �         �  �          �  �  �  �     
      W  w  �              �  w   �  �       �         T   �      �   ,   �  �   *   �  =           S      �   �   ]   9   l   �  r   �   �    �          �  @       V  &  /  K      q    &  �  x   �             �  4  )  ^         �           �    ,  �  #                 H             u  M     �  3       �         �       �      V   �   �   �   (       D      6  �   �       �  �      �   �          <   L  G  �   �       N   �    �   �  �   f             v       �  `  �           )  �       �  9      '       �   .   �   0      C   D   E   F   G           �   �  �  d   B   P      ~  ]  �  c  |       X      �  B  �   �       �           �       �   �   �   �       �   �      Y  �      >  �   z   ?   �          �   �  �  h   I      �   
   ?  %   �      7         "  �  �   �   @  �        <  2   �  �         �    ^  �  M          :          N  �      �      $   [    l        -  �  �   �     �      �   �   �          m   n   o   p   0   A        7  �  �          e   �  �   �       	  �       �      �      �     �  �   �  q   }  $  �   �       �      �    �   �  �  �       s   U         >   �   ~   )   1   5   !  f  X   �  �   �   �  2  �             �  T  j   �  �           Y     �   �        �           �      "      `   �   �  �   �         �    �  �  }   �  �  �   �     �      y    n  �   �   �    �   �  �      �  �  .  /       c      O   �    
Bibus can directly insert citations in OpenOffice.org
if you start OpenOffice.org in 'listening mode'.


Do you want to activate this mode by default?


  1. A document will open in OpenOffice.org,
  2. Click on 'Accept UNO connections',
  3. Quit OpenOffice (and the Quickstarter if used),
  4. Restart OpenOffice.org.  and order them by % %(total_number)s reference(s) : %(number_selected)s selected %s connection parameters ,  , et al. - - If you want to use Bibus on a single computer (mono-user), 
            SQLite is presumably the best choice. - If you want to use Bibus on multiple computers (multi-user),
            MySQL is presumably the best choice. - You can change this choice at anytime in the preferences. -- 3306 :  ;  ARTICLE Abbreviate author list with Abstract Acrobat file Activate Add Add Text Add a field Add a line Add child Address Advanced preferences After All All files All my references All references Annote Anonymous Anonymous ... Anonymous author format As in database Ascending Author Author "%(author)s" contains %(nb_comma)s comma.
The correct format for authors is:

"Name, FirstName MiddleName"
or
"Name, FM"

Would you like to re-edit the reference? Author "%(author)s" contains a period.
The correct format for authors is:

"Name, FirstName MiddleName"
or
"Name, FM"

Would you like to re-edit the reference? Author "%(author)s" is very long.
The correct format for authors is:

"Name1, FirstName1 MiddleName1; Name2, FirstName2 MiddleName2"
or
"Name1, FM; Name2, FM"

Would you like to re-edit the reference? Author list Author list format Author-Date BOOK BOOKLET Base Style Because of an error, I didn't format reference type %(typeName)s and field %(fieldName)s Before last BibTeX Bibliographic index BibliographicType Bibliography title Bibus MySQL Setup Bibus WEB site Bibus can directly insert citations
in you favorite word processor Bibus needs a USERNAME to identify yourself.
Please enter a name in the box below.
Your login name might be a good idea Bibus uses a file to store the bibliographic database.

Please enter in the box below the path to 
the bibliographic database you want to use.

- If the file EXIST, Bibus will 
	- not modify it
	- assume it to be a correct Bibus database.

- If the file DOES NOT EXIST, Bibus will 
	- create it
	- use it as your bibliographic database. Black Blue Bold Booktitle Brackets CONFERENCE CUSTOM1 CUSTOM2 CUSTOM3 CUSTOM4 CUSTOM5 Cancel Cannot connect to Openoffice. See Documentation. Cannot connect to Openoffice. See Documentation.
%s
%s) Caps Capture Citations Case Change author list Chapter Check database Check for duplicates Choose a File to import Choose the destination file Choose the file location and name Choose the name of the SQLite database Citation Cited Clean database ... Clear Codecs Common Connect Connect to database Connect... Connection Connection type Content Copy Count Create Index on Insert Create User Create a new category Create the Bibliography if absent Create user... Creating styles ... Creator Current key Custom1 Custom2 Custom3 Custom4 Custom5 Cut Cyan DB engine Database Database = %s Database = None Database Creation Database Error Database Name Database Type Database check Database choice Database cleanup Database cleanup may corrupt your database!
You should make a backup of your database before using this command. Database conversion Database engine Database error Database file ... Database file... Database type Database used at startup Database... Date Delete Delete journal Delete query Delete reference Delete the selected reference Delete this key Deleting old index and citations ... Descending Details Directory ... Display Displayed fields Document position Documentation Done Down Download styles Duplicate reference Duplicates EMAIL Edit Edit ... Edit Identifier Edit journal Edit query Edit reference Edit the selected reference Edition Editor Empty Trash Empty Trash and DB cleanup Enter your connection parameters below Enter your email below (Optional): Error Error during query Error in authors? Expert Expert Search Export Export a file Export formatted references in an HTML file Export to BibTeX format Export to Medline format Export to RIS format Export to Refer format for EndNote Export to a SQLite database Field Field Value Field color Field name Field name color Fields Fields formatting Fields ordering File File ... File error operation File name ... Fill character in tab stops Finalize Finalize the document Finalize... Finalizing the current document First Connection Wizard First Editor First author First key Format Format Author as Format Bibliography Format Editor as Format fields as Format reference as Formating citations ... Full Full journal name Fuse: [1] [3] [2] -> [1; 3; 2] Fuse: [Martin] [John] -> [Martin; John] Fusing citations ... GoTo Writer Grants Green HTML Help Hilight Citations Hilight citations with a yellow background Hitting "Detele" will remove all the references from the "Non-classified" key.
Some of the references may not be deleted if they are in use by another user. Hitting "Detele" will remove all the references from the Trash key.
Some of the references may not be deleted if they are in use by another user. Host Howpublished INBOOK INCOLLECTION ISBN ISI ISO ISO journal abbreviation Id Identifier If a duplicate is found If at least If author Field is empty, replace it with: If you have the necessary rights (MySQL Administrator),
click the button below to create/change the MySQL database Import Import && Quit Import a BibTeX file Import a Medline XML file Import a Medline file Import a RIS file Import a Refer file Import a Refer file exported from EndNote Import a file Import a text file Import an ISI Web of science file Import an XML library generated with Endnote 9 or Later Import buffer Incorrect position Information Insert Insert Citation Inserting citations in text Inserting citations in text (%s ranges) Institution It seems that your database is not compatible with your PySQLite version.
If it does not work,
see the SQLite site about database format change in SQLite3 http://www.sqlite.org/version3.html Italic JOURNAL Joining Journal Journal Abbreviation Journal abbreviations Journals Key type Language Last Editor Last author License Light_grey List all authors on first occurence if authors number is not higher than List more authors until it is unique Load ... Looking for duplicates ... MANUAL MASTERTHESIS MISC MS Word MSWord Main Main Fields Main Fields = %s Main fields (+ Abstract) Making a copy of the current document ... Mark the selected reference Medline Medline Search MedlineXML Microsoft Word Microsoft Word Document Middle Editors Middle authors Mode Modified by Modify User Grants Modify user... Month MySQL MySQL database MySQL database setup ... MySQL setup MySQL v3 and v4 MySQL v4.1 and v5 Name Name format New New ... New Database... New Name format New Query New journal New query New reference NewKey No Choice No Database selected Non allowed operation Non-classified None Norma Jean Baker Normal Normal (rw) Not connected Not sorted Note Nothing Number Number entries Number of records Numbering OK OOo_pipe Open URL OpenDocument Text OpenOffice connection settings OpenOffice.org OpenOffice.org Text Document OpenOffice.org connection OpenOffice.org connection settings Organizations Other Fields PHDTHESIS PROCEEDINGS Page Setup Pages PassWord Password Paste Paths Pipe Pipe name Please choose below the Word Processor you want to use with Bibus.
You can change this setting at any time in Bibus menu Edit/Preferences. Please choose the bibliographic database Please choose the database engine you want to use Please choose the file encoding Please enter password for database %s Please select a style file Please select the file Please, enter the new menu name Please, enter the new name Please, enter the new query name Please, enter the query name Please, select the default directory Port Position Position of the tab stop (mm) Preferences Preview Preview ... Preview : Preview printing Print ... Print references Printed fields Printing PubMed PubMed journal name PubMed search Publisher Pubmed search ... Queries Quit Quit Bibus RIS Read-only with private and shared trees (ro) Read-only with shared tree only (rk) Read-only without tree (rr) Red Refer Refer (EndNote) Reference Reference Editor Reference display Reference fields Reference list Reference type References References formatting Remove Rename Query Rename folder Rename query Rename this key Report_Type SQLite SQLite file SQLite setup SQlite database file Save Save as... School Search Search && Close Search ... Search PubMed Search Pubmed on the Web Search in Search the database Second key Select All Selection Separate authors with Separator 1 Separator 2 Separator 3 Separators Separators ... Series Set printer page format Set some preferences Setting database and rights Shared Show All Small Caps Socket Solving duplicates ... (%s series of duplicates) Some needed fields are absent from the selected database/tables.
Continue at your own risk Some references were not formatted. Are you sure they are in the database? Sorry but codec '%s' is not available.
I will try to use ascii Sorry but the MySQL python module (MySQLdb) is not available.
Read installation.txt Sorry but the SQLite python extension is not available.
Read installation.txt Sorry, I can't fix this error. Sorry, but I was not able to find the python module for the %s database.
Please check your installation Sorry, you cannot move a reference associated with this key. Sort Ascending Sort Descending Sort bibliography by Sort: [1] [3] [2] -> [1] [2] [3] Sorting Standard Start at record Style Style ... Style author Style creation date Style format Error Style informations Style modification date Style name Styles Subscript Superscript Supplementary Fields Supplementary fields TCP/IP TECHREPORT Tab stop is right aligned Table "%(name)s" is absent from the database.
Should I fix it? Tabulation Tag Tag reference Tagged Template Text Window The PubMed journal name must be non empty The database contains references with NULL Identifiers
Should I delete them ? The front document in OOo must be a Writer document. The key must be unique.
Please choose a new key name. The reference or the identifier was not unique. The reference identifier has been changed %s The reference or the identifier was not unique. The reference identifier has been changed to %s The selected style is not a bibus style The style name is not correct, please avoid: '/' under linux; '\' and ':' under Windows The version of the style file is too old.
 I will convert it to the new Bibus style format.
To get rid of this message, please save it again. The version of the style file is too recent. Please update bibus to a new version.
 I will open a default style. Third key Title UNPUBLISHED URL URL/File... Underline Unknown Up Update Bibliographic Index Update Citations Update Index on Insert Update and Pre-format all... Update the Bibliography automatically Updating index ... Updating references ... Use Database: Use OpenOffice.org format for printing Use TCP/IP Use a pipe Use current language for month Use range: [1] [3] [2] -> [1-3] User User Creation User Name UserName Username Using separator Volume WWW Warning Welcome to Bibus ! Welcome to bibus What do you want to export? Where to save the style file? White Word Processor XML From Endnote Year You cannot edit default styles
.I have made a copy of the style for editing You cannot use '%' and '_' characters.
Please choose a new key name. You database has been converted to the new format.
The database is now located at
"%s" You did not select a valid database You must first save the current document before using this function.
 Should I save it and proceed? Your database is a valid bibus database. ]-[ a Field a String ascii authors authors, replace authors 2 to n with below biblioDB case sensitive cp1252 default in 'ARTICLE' in database keep the new reference keep the old reference last latin-1 localhost root save password (Not secure!) shortcut using separator utf-8 when there are more than Project-Id-Version: bibus-1.4
POT-Creation-Date: 2008-05-17 21:29+0100
PO-Revision-Date: 2008-10-09 14:18+0300
Last-Translator: azure <azure@fast.net.ua>
Language-Team: ru <azure@fast.net.ua>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: utf-8
X-Poedit-Basepath: .
X-Poedit-Language: Russian
X-Poedit-Country: RUSSIAN FEDERATION
X-Poedit-SearchPath-0: ..
 
Bibus может непосредственно вставлять цитаты в OpenOffice.org
есои Вы запустите OpenOffice.org в 'listening mode'.


Хотите активировать этот режим по-умолчанию?


  1. В OpenOffice.org откроется документ,
  2. Кликнуть 'Accept UNO connections',
  3. Выйти из OpenOffice (и из Quickstarter, если запущен),
  4. Запустить OpenOffice.org. и сортировать их по % %(total_number)s reference(s) : %(number_selected)s выбрано %s параметры подключения , . et.al. - - Если хотите использовать Bibus на одном компьютере.
   SQLite - лучший выбор - Если хотите использовать Bibus на нескольких компьютерах (несколько пользователей).
       MySQL - лучший выбор -В любой момент можно изменить это в настройках -- 3306 : ; СТАТЬЯ Заканчивать список автором Тезис Файл Acrobat Активировать Добавить Добавить текст Добавить поле Добавить строку Добавить категорию Адрес Детальные настройки После Все Добавить файлы Мои ссылки Все ссылки Аннотация Anonymous Anonymous... Формат анонимного автора Как в базе данных По возрастанию Автор Автор "%(author)s" содержит %(nb_comma)s запятых.
Правильный формат для авторов:
"Фамилия, Имя Отчество;"
или
"Фамилия, ИО;"
Хотите исправить? Автор "%(author)s" содержит точку.
Правильный формат для авторов:
"Фамилия, Имя Отчество;"
или
"Фамилия, ИО;"
Хотите исправить? Автор "%(author)s" слишком длинный.
Правильный формат для авторов:
"Фамилия, Имя Отчество;"
или
"Фамилия, ИО;"
Хотите исправить? Список авторов Формат списка авторов Автор-Дата КНИГА БУКЛЕТ Базовый стиль Из-за ошибки не удалось отформатировать ссылку типа %(typeName)s и поле %(fieldName)s Перед последним BibTeX Индекс библиографии Тип Заголовок библиографии Настройка БД MySQL Bibus WEB site Bibus может непосредственно вставлять ссылки
в ваш любимый текстовый процессор Необходимо указать USERNAME для того чтоб идентифицировать себя.
Введите имя в поле.
Скорее всего это ваш логин Bibus использует файл для хранения БД библиографии.

Пожалуйста, введите путь который Вы хотите использовать 
для хранения базы данных библиографии.

- Если файл СУЩЕСТВУЕТ, Bibus: 
	- не изменит его
	- будет считать его корректной БД Bibus. 

- Если файл НЕ СУЩЕСТВУЕТ, Bibus: 
	- создаст его
	- будет использовать как БД библиографии. Черный Синий Жирный Название книги Скобки КОНФЕРЕНЦИЯ ПОЛЬЗОВАТ1 ПОЛЬЗОВАТ2 ПОЛЬЗОВАТ3 ПОЛЬЗОВАТ4 ПОЛЬЗОВАТ5 Отмена Не могу соединиться с OpenOffice. См. документацию. Не могу соединиться с OpenOffice. Смотри в документации.
%s
%s) ПРОПИСНЫЕ Захват цитат Регистр Изменить список авторов Глава Проверить БД Проверить наличие дубликатов Выберите файл для импорта Выберите файл для экспорта Выбрать расположение и имя файла Выберите имя БД SQLite Цитирование Процитированные Очистить БД... Очистить Кодеки Общее Соединение Соединиться с БД Подключение... Соединение Тип соединения Содержанию Копировать Количество Создать индекс при вставке Создать пользователя Создать новую категорию Создать библиографию, если ее нет Создать пользователя Создание стилей Создатель Текущий ключ Пользоват1 Пользоват2 Пользоват3 Пользоват4 Пользоват5 Вырезать Голубой Движок БД БД БД = %s База данных = Нет Создание БД Ошибка БД Имя БД Типа БД Проверка БД Выбор БД Очистить БД Очистка БД может повредить её!
Вам следует сделать резервную копию БД перед использованием этой команды. Преобразование БД Движок БД Ошибка БД Файл БД... Файл БД Тип БД БД при запуске БД... Дата Удалить Удалить журнал Удалить запрос Удалить ссылку Удалить выбранную ссылку Удалить ключ Удаление старого индекса и цитат По убывания Подробнее Папка... Вид Отображаемые поля Положению в документе Документация Выполнено Вниз Загрузить стили Дублировать ссылку Дубликаты EMAIL Правка Правка... Редактировать идентификатор Редактировать журнал Править запрос Правка ссылки Правка выбранной ссылки № редакции Редактор Очистить корзину Очистить корзину и оптимизировать БД Введите параметры соединениея Введите свой email (не обязательно) Ошибка Ошибка в запросе Ошибка в авторах? Эксперт Расширенный поиск Экспорт Экспорт в файл Экспорт форматированных ссылок в HTML Экспорт в формат BibTeX Экспорт в формат Medline Экспорт в формат RIS Экспорт в формат Refer для EndNote Экспортировать в SQLite Поле Значение поля Цвет поля Название поля Цвет названия полей Поля Форматирование полей Порядок полей Файл Файл... Ошибка при операции с файлом Имя файла... Запонять символом Завершить Завершить документ Завершить... Завершение текущего документа Мастер настройки соединения Первый редактор Первый автор Первый ключ Формат Форматировать автора как Форматировать библиографию Форматировать редакторов Форматировать поля как Форматировать ссылку как Форматирование цитат... Полностью Полное название журнала Слияние: [1] [3] [2] -> [1; 3; 2] Слияние: [Martin] [John] -> [Martin; John] Собираем цитаты... Открыть Writer Разрешения Зеленый HTML Помощь Подсветка цитат Подсветка цитат желтым фоном Нажатие на кнопку "Удалить" приведет к удалению всех не классифицированных ссылок.
Некоторые ссылки могут не удалиться, если они используются другим пользователем Нажатие на кнопку "Удалить" приведет к удалениею всех ссылок из Корзины.
Некоторые из ссылок могут не удалиться, если они используются другим пользователем. Host Способ публикации В КНИГЕ ИЗ КОЛЛЕКЦИИ ISBN ISI ISO Аббревиатура журнала (ISO) Id Идентификатор Если дубликат найден Если по меньшей мере Если поле "Автор" пусто, заменять на: Если у Вас есть достаточные права (администратор MySQL),Вы можете создать/изменить БД MySQL Импорт Импорт && Выход Импорт из BibTeX Импорт файла Medline XML Импорт файла Medline Импорт файла RIS Импорт файла Refer Импорт файла Refer, экспортированного из EndNote Импорт из файла Импорт текстового файла Импорт файла ISI Web of science Импорт XML-библиотеки, сгенерированной Endnote 9+ Буфер импорта Неверная позиция Информация Вставка Вставить ссылку Вставка цитат в текст Вставляем цитаты в текст (%s диапазонов) Институт Похоже, что Ваша БД не совместима с Вашей версией PySQLite.
Если она не работает,
см. сайт SQLite на предмет смены формата в SQLite3 http://www.sqlite.org/version3.html Курсив ЖУРНАЛ Объединение Журнал Сокращение журнала Аббревиатура журнала Журналы Тип ключа Язык Последний редактор Последний автор Лицензия Серый Перечислять всех авторов при первом появлении, если число автором не более Перечислять авторов до повторения Загрузить... Поиск дубликатов... РУКОВОДСТВО ДОКТ. ДИССЕРТ. ДРУГОЕ M$ Word M$ Word Главная Основные поля Главные поля = %s Основные поля (+ Тезисы) Создание копии текущего документа Выделить выбранную ссылку Medline Поиск Medline MedlineXML Micro$oft Word Документ Microsoft Word Промежуточные редакторы Средние авторы Режим Редактировано Изменить права пользователя Редактировать пользователя Месяц MySQL БД MySQL Настройка БД MySQL Установка MySQL MySQL v3 и v4 MySQL v4.1+ Название Название формата Новый Новый... Новая БД Формат нового имени Новый Запрос Новый журнал Новый запрос Новая ссылка НовыйКлюч Нет выбора Не выбрана БД Недопустимое действие Не классифицированные Нет Пушкин Александр Сергеевич Нормальный Обычные (rw) Не подключен Не отсортирован Заметка Ничего Номер Нумеровать ссылки Число записей Нумерация Ок OOo_pipe Перейти по URL OpenDocument Text Настройка соединения OpenOffice OpenOffice.org OpenOffice.org Text Document Соединение OpenOffice.org Настройки соединения OpenOffice.org Организация Другие поля КАНД. ДИССЕРТ. ТРУДЫ Параметры страницы Стр. Пароль Пароль Вставить Пути Pipe Pipe name Пожалуйста, выберите текстовый процессор для использования с Bibus.
Вы сможете изменить это в настройках. Выберите БД библиографии Выберите движок БД Выберите кодировку файла Введите пароль для БД %s Выберите файл стилей Выберите файл Введите название нового меню Введите новое имя Введите название нового запроса Введите имя запроса Пожалуйста, укажите папку по умолчанию Порт Расположение Отсутп (мм) Параметры Предпросмотр Просмотр... Предпросмотр: Просмотр печати Печать... Печатать ссылки Печатаемые поля Печать PubMed Название журнала (PubMed) Поиск PubMed Издатель Поиск Pubmed... Запросы Выход Выйти из bibus RIS Только для чтения с собственным и общими деревьями (ro) Только для чтения с общим деревом (rk) Только для чтения бед дерева (rr) Красный Refer Refer (EndNote) Ссылка Редактор ссылок Отображение ссылок Поля ссылок Список ссылок Тип ссылки Сылки Форматирование ссылко Удалить Переименовать Запрос Переименовать папку Переименовать запрос Переименовать ключ Тип отчета SQLite Файл SQLite Установка SQLite файл БД SQlite Сохранить Сохранить как Школа Поиск Найти и закрыть Поиск... Поиск PubMed Искать в Pubmed через Web Поиск в Поиск по БД Второй ключ Выделить все Выделение Разделять авторов Разделитель 1 Разделитель 2 Разделитель 3 Разделители Разделители Серия Формат бумаги Установить параметры Выбрать БД и права Общие Показать все маленькие прописные Socket Развязка дубликатов... (%s серий дубликатов) Некоторые требуемые поля отсутствуют в таблице БД.
Рекомендуется прекратить работу. Некоторые ссылки не были отформатированы. Вы уверены, что они есть в БД? Кодек '%s' недоступен.
Попробую использовать ASCII Модуль MySQL для python (MySQLdb) недоступен.
Подробней в installation.txt Расширение SQLite для python недоступно.
Подробнее в installation.txt Не удалось исправить ошибку Не найден модуль %s для python.
Проверьте правильность установки Извините, вы не можете переместить ссылку, связанную с этим ключом Сортировать по возрастанию Сортировать по убыванию Сортировать библиографию по Сортировать [1] [3] [2] -> [1] [2] [3] Сортировка Стандарт Начать с записи Стиль Стиль... Стиль автора Стиль даты создания Ошибка форматирования стиля Информация о стилях Стиль даты модификации Имя стиля Стили Нижний индекс Верхний индекс Дополнительные поля Дополнительные поля TCP/IP ТЕХ. ОТЧЕТ Отступ табуляци справа Таблица "%(name)s" отсутствует в БД.
Исправить? Табуляция Отметка Отметить ссылку Помеченные Шаблон Текстовое Окно Название журнала (PubMed) не может быть пустым БД содержит ссылки с пустым идентификатором
Удалить их? Текущим документом в OOo должен быть документ Writer Ключ не должен повторяться.
Выберите другое имя Ссылка или идентификатор не были уникальны. Идентификатор ссылки был изменен на %s Ссылка или идентификатор не были уникальны. Идентификатор ссылки был изменен на %s Выбранный стиль не является стилем bibus Некорректное имя стиля, пожалуйста, избегайте символов '/' в GNU/Linux; '\' и ':' в Windows Версия стиля слишком старая.
Она будет конвертирована в новый формат стиля Bibus.
Пожалуйста, пересохраните этот стиль чтобы избежать появления этого сообщения. Версия стиля слишком новая. Пожалуйста, обновите bibus до новой версии.
Буду использовать стиль по-умолчанию Третий ключ Название НЕОПУБЛИКОВАННОЕ URL URL/File... Почеркнутый Неизвестно Вверх Обновить индекс библиографии Обновить цитаты Обновлять индекс при вставке Обновить и форматировать все Автоматически обновлять библиографию Обновление индекса Обновление ссылок Использовать БД: Использовать формат OpenOffice.org для печати Использовать TCP/IP Использовать pipe Использовать месяц из текущей локали Использовать диапазон: [1] [3] [2] -> [1-3] Логин Создание пользователя Имя пользователя Логин Логин Используя разделитель Том WWW Предупреждение Добро пожаловать в Bibus! Добро пожаловать в bibus Что вы хотите экспортировать? Куда сохранить файл стиля? Белый Текстовый процессор XML из Endnote Год Вы не можете редактировать стили по-умолчанию Вы не можете использовать символы '%' и '_'.
Выберите другое имя ключа Ваша БД была преобразована в новый формат.
БД теперь находится 
"%s" Вы не выбрали подходящую БД Перед использованием этой функции Вы должны сохранить текущий документ.
Сохранить и продолжить? Ваша БД соответствует требованиям bibus ]-[ Поле Строка ascii авторов авторов, заменять авторов от 2 до n ниже biblioDB case sensitive cp1252 по умолчанию в СТАТЬЕ в базе данных использовать новую ссылку оставить старую ссылку последняя latin-1 localhost root помнить пароль (ОПАСНО!) ярлык используя разделитель utf-8 если более 