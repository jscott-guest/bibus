# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#


import copy
from BIBbase import *

# CONST is the list of the constant in BIBbase that are saved in BibConfig
# and that may be different for each window
# each BibFrame instance will have it own copy
# we just need to list the [] and {} types
# since only these types may modify the values in BIB
CONST = [EDIT,
		PRINTER_FORMAT]

for attr in CONST:
	attr = copy.deepcopy(attr)
	
