# Written by Leon Commandeur <leon_commandeur@hotmail.com>

Here's a 'short' guide on how to install Bibus0.9.0 using SQLite in combination with OpenOffice 1.1.2 on a WindowsXP system.

First, download all the necessary programs to e.g., c:\downloads. These are:
1. A file extraction program: winzip90.exe (evalutation version) from http://winzip.com/downauto.cgi?file=winzip90.exe
2. The OpenOffice Productivity Suite: OOo_1.1.2_Win32Intel_install.zip from http://download.openoffice.org/1.1.2/index.html
3. The Programming language Bibus uses: Python-2.2.3.exe from http://www.python.org/2.2.3/
4. An extensions for Python: xPythonWIN32-2.5.2.8u-Py22.exe from http://prdownloads.sourceforge.net/wxpython/wxPythonWIN32-2.5.2.8u-Py22.exe
5. The Database system: sqlite-3_0_7.zip from http://www.sqlite.org/download.html
6. The Bibus program: bibus-0.9.0.tgz from http://prdownloads.sourceforge.net/bibus-biblio/bibus-0.9.0.tgz?download
7. A database exchange extension: sqliteodbc.exe from http://www.ch-werner.de/sqliteodbc/
8. A OpenOffice setup file: Setup.xcu from http://bibus-biblio.sourceforge.net/
You now have all the necessary files on your computer.

Second, prepare windows for the new program and their interactions.
1. Go to System Propertries by right clicking on 'My Computer' and selecting 'properties'.
2. Go to the 'Advanced' tab
3. Click on the 'Environment Variables' button
4. Click on the 'New' button below the 'User variables for <user>' window
5. Enter as variable name: PATH
   Enter as variable value: C:\Program Files\OpenOffice.org1.1.2\program
6. Click on the 'OK' button
7. Click on the 'New' button below the 'User variables for <user>' window
8. Enter as variable name: PYTHONPATH
   Enter as variable value: C:\Program Files\OpenOffice.org1.1.2\program
9. Click on the 'OK' button (3 times)
The system is now ready to let the programs interact.

For the following steps, one needs 'Administrator' privileges. This might mean that you will have to log off as the current user and login as the administrator.

Third, install the programs

First, WinZip. This will allow you to extract the previously downloaded files.
1. Double click Winzip.exe
2. Press 'Setup'
3. Press 'OK'
4. Press 'Yes' (if you agree with the license agreement)
5. Press 'Next'
6. Press 'Express Setup'
7. Press 'Finish'
You should now have the WInzip program installed.
Now, open Winzip (from the start menu) and go to the 'Options'>'Configuration' menu. Under the 'Miscellaneous' tab, uncheck 'TAR file smart CR/LF conversion' and press 'OK'.
Winzip is now ready to be used.

Second, OpenOffice.
1. Double click OOo_1.1.2_Win32Intel_install.zip, this will open WinZip and display a long list of files.
2. Use the 'Extract' button to extract these files
3. In the 'Extract to' field, enter c:\Program files\OpenOffice.org1.1.2. Make sure that the 'all files/folders in archive' option is checked.
4. Press 'Extract', Winzip will now extract the files. When winzip is done, you can close the program.
5. Open 'My Computer' and go to C:\Program Files\OpenOffice.org1.1.2\OOo_1.1.2_Win32Intel_install
6. Double click the 'setup.exe' file
7. Press 'Next' (twice)
8. Read all of the licence agreement (scroll all the way down), check 'I accept the terms of the Agreement', and click on 'next'
9. Enter the field you like, and click 'next'
10. Click 'Next', 'Next', 'Install', 'OK', and 'OK'. OpenOffice is now installing.
11. Click 'Complete' and you have installed the OpenOffice Productivity Suite.
12. You can now remove the folder C:\Program Files\OpenOffice.org1.1.2\OOo_1.1.2_Win32Intel_install

Third, Python, this will allow Bibus to execute sane arguments.
1. DOuble click python-2.2.3.exe
2. Press 'Next' five times and Python installs
3. Press 'Finish' and Python is installed

Fourth, the python extension, this will make Bibus arguments sound solid as well ;)
1. Double click wxPythonWIN32-2.5.2.8u-Py22.exe
2. Click 'Next', 'Yes', 'Next' (three times), and the extension installs.
3. Leave all three options checked, and read the README if you're interested. The extension will now run some conversions.
The python extrensions are now installed

Fifth, the SQLite database
1. Double click sqlite-3_0_7.zip
2. Extract it to C:\Python22

Sixth, the Bibus program itself
1. Double click bibus-0.9.0.tgz
2. Winzip opens and asks to decompress it in a temporary folder, press 'Yes'
3. Extract it to C:\Program files\Bibus
4. Close Winzip.
5. Double click setup.py in C:\Program Files\Bibus\Bibus-0.9.0
6. Right click BibusWin.pyw and select 'Send to...'>'Desktop (create shortcut)'

And Seventh, the ODBC link,
1. Double click sqliteodbc.exe
2. Check 'I agree with the above terms and conditions' and click 'Next'
3. Click 'Start' and if all goes right 'OK'.

Everything is now installed.

Fourth, setting up the programs so that they work correctly.
1. Copy the downloaded Setup.xcu file to C:\Program Files\OpenOffice.org1.1.2\share\registry\data\org\openoffice and click 'OK' to replace the file.
2. Double click the 'Shortcut to BibusWin.pyw' on your desktop
3. Enter a user name and database file name and location for the database file.

Congratulations, if everything went according to plan, you now have a working multi user bibliographic expansion to OpenOffice!!!

Further documentation on how to actually use Bibus can be found on http://bibus-biblio.sourceforge.net/bibus_doc.html

Good luck and enjoy,

Leon Commandeur
