Creation date: 2004.05.18
Last revision: 2004.06.02

**************
** Preamble **
**************
1) You must have the python-uno bridge installed. It is normally installed by default if you use OpenOffice.org > 1.1
	a) The python included with OpenOffice.org is in $OO/program directory where $OO is normally '/usr/local/' if you use the official installer.
	b) Extension should be installed in $00/program/python-core/lib/
2) This is however tricky because this python installation is far from complete. In particular, the headers are absent and you cannot use distutils.
3) In order not to mess up your OpenOffice.org installation, I think it is a good idea to install everything in another place. If you do it this way, upgrading OpenOffice.org or Bibus should not be a problem anymore. This "second" installation will just be used for python and you will continue to use your regular OpenOffice.org software.
4) Depending on the database engine you want to use you of course just need to insert python extension and odbc extensions for this database (MySQL <http://www.mysql.com/> or SQLite <http://www.sqlite.org/>).

******************
** Installation **
******************
0) Do a minimun install of OpenOffice.org1.1.1 in $OO
	I use $OO=/opt/OpenOffice.org
	Choose 'Custom Install' and
	Install only the python-uno bridge. You don't need anything else. Uncheck Writer, Calc, etc...
	The install will be around 180 Mo. Many files can presumably deleted by hand, but I did not experiment with this.
	If you prefer not to duplicate your OpenOffice.org installation use the path to you OpenOffice.org install for $OO below
1) We want to transform the python installation in $OO in a completely functional python installation.
	a) Download the python source from www.python.org.
	Use the same version than the one included in $OO (It should be 2.2.2)
	I downloaded 2.2.3 wince it is just a bug fix = Python-2.2.3.tgz
	b) tar -zxf Python-2.2.3.tgz ; cd Python-2.2.3
	c) ./configure
	d) If there is no error, the configure script should have generated:
		pyconfig.h
		Makefile
	e) 	mkdir $OO/program/python-core/
		mkdir $OO/program/python-core/python2.2
		mkdir $OO/program/python-core/lib/python2.2
		mkdir $OO/program/python-core/lib/python2.2/config
	f) 	cp Python-2.2.3/Include/* $OO/program/python-core/include/python2.2/
		cp pyconfig.h $OO/program/python-core/include/python2.2/
		cp Makefile $OO/program/python-core/lib/python2.2/config/
	g) You should now have a working python installation in $OO/program/python-core/
	and you can delete the Python-2.2.3 directory.

2) Optional: Define somewhere in you path the 'pyuno' command in order to easily start the correct python version.
I installed in my $HOME/bin directory a symbolic link :
ln -s $OO/program/python pyuno
Now the correct python interpreter can be started by typing pyuno.

3) The pyuno interpreter may not find some libraries on your system. For instance it couldn't find libssl.so on my machine because it does not exist under this name. Under my unstable Debian there is a /usr/lib/libssl.so.0.9.7 library. I thus did as root:
	cd /usr/lib
	ln -s libssl.so.0.9.7 libssl.so.0
	ln -s libssl.so.0 libssl.so
I had the same problem with the libcrypto library and solved it the same way.
Your mileage may vay depending on your favorite GNU/linux distribution.

The rest of the installation procedure is now done the normal way for the python extension used. Of course you must take car to use the correct python interpreter, that is $OO/program/python. The best is to read the doc of the extensions. I give below what I did in my case.

**************
** wxPython **
**************
Bibus has been developped with wxPython2.4.2.4
I have tested with the newly release 2.5.1.5 version and it works except for some minor glitches (Error messages are displayed in the console instead of a dialog. I will try to fix that. It has presumably something to do with wxLog which is not correctly initialized to wxLogGui as needed. If you know how to correct this, please let me know).
The following procedure will compile and install wxPython with unicode and gtk2 support.

This is from wxPythonSrc-2.4.2.4/README.1st.txt
1) export WXPREF=/opt/wxPython
2) tar -zxf wxPythonSrc-2.4.2.4.tgz
2) cd wxPythonSrc-2.4.2.4
3) mkdir build #if it does not exist !
4) cd build
5) ../configure --with-gtk --prefix=$WXPREF --enable-rpath=$WXPREF/lib --with-opengl --enable-geometry --enable-optimise --enable-debug_flag --enable-gtk2 --enable-unicode
6) make
7) as root : make install	# root needed to have write permission to $WXPREF
8) cd ../wxPython
9) as root :
	export WXPREF=/opt/wxPython
	$OO/program/python setup.py WX_CONFIG=$WXPREF/bin/wx-config WXPORT=gtk2 UNICODE=1 build install

******************
** MySQL-python **
******************
You need MySQL developement files

1) http://sourceforge.net/projects/mysql-python. Download MySQL-python-0.9.2.tar.gz
2) tar -zxf MySQL-python-0.9.2.tar.gz ; cd MySQL-python-0.9.2
3) $OO/program/python setup.py build
4) as root : $OO/program/python setup.py install

***************
** PySQLlite **
***************
1) http://pysqlite.sourceforge.net/. Download pysqlite-0.5.0.tar.gz
2) tar -zxf pysqlite-0.5.0.tar.gz ; cd pysqlite-0.5.0
3) $OO/program/python  setup.py build
4) as root :
	$OO/program/python setup.py install

***********
** pyXML **
***********
Because the python included in OpenOffice.org does not contain expat, you need to install it.
The easiest way is to install pyXML which contains expat.

1) http://pyxml.sourceforge.net/
2) tar -zxf PyXML-0.8.3.tar.gz
3) cd PyXML-0.8.3
4) $OO/program/python setup.py build
5) as root
	$OO/program/python setup.py install

**********
** ODBC **
**********
If you want to be able to use the classical OpenOffice.org interface to insert references, you must install odbc.

This is presumably available in your linux distribution. In debian you need to install
	libmyodbc - MySQLodbc driver
	odbcinst1 - Support library and helper program for accessing odbc ini files
	unixodbc - ODBC tools libraries

For MySQL : http://dev.mysql.com/doc/mysql/en/ODBC.html
For SQLite : http://www.ch-werner.de/sqliteodbc/

It is then easy to configure odbc using for instance gODBCConfig.

On my system :
*******************
** /etc/odbc.ini **
*******************
empty file

***********************
** /etc/odbcinst.ini **
***********************
[MySQL]
Description		= MySQL driver
Driver		= /usr/lib/odbc/libmyodbc.so
Setup		= /usr/lib/odbc/libodbcmyS.so
CPTimeout		=
CPReuse		=
FileUsage		= 1

[SQLite]
Description=SQLite ODBC Driver
Driver=/usr/local/lib/libsqliteodbc.so
Setup=/usr/local/lib/libsqliteodbc.so

*****************
** ~/.odbc.ini **
*****************
[MySQL-Biblio]
Description	= MySQL Bibliographic database
Driver		= MySQL
Server		= localhost
Database	= Biblio

[Test]
Description	= SQLite
Driver		= SQLite
Database	= /home/pmartino/biblio.sqlite
Timeout		= 2000
StepAPI		= No



