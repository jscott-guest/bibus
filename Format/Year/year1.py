# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
import time
#
def format(s,uselocale,f=0):
	"""Return year as
	1998 if f=0
	98 if f=1"""
	#
	s = s.strip()
	if f == 0: format = '%Y'
	else: format = '%y'
	#
	try:
		return time.strftime(format,time.strptime(s,'%Y'))	# we first try full year
	except ValueError:
		try:
			return time.strftime(format,time.strptime(s,'%y'))	# or just 98 ?
		except ValueError:
			return s					# can't do anything
