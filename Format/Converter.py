# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
from bibOOo.CONST import SEP, BIB_TYPE,BIB_FIELDS
VERSION = 1.0	# converter version used by this file
#
# set default value
# format['version'] = Converter version = here 1.0
# format['locale'] = True/False	# use locale setting / english
# format = conv[TYPE][FIELD] = (('module1',parameters1),('module2',parameters2),...)
# if no conversion needed = ((None,any),)
#
def_conv={}
def_conv['version'] = VERSION
def_conv['locale'] = False
for typ in BIB_TYPE:
	def_conv[typ]={}
	default_conv = {}
	for field in BIB_FIELDS:
		default_conv[field] = ((None,),)
	default_conv['Author'] = (('Format.Author.Author.author4',(' ','','','','')),\
					('Format.Author.Author.author4',(' ','','','','')),\
					('Format.Author.Author.author4',(' ','','','','')),\
					('Format.Author.Joining.joining1',('',', ',', ',', ','',0,'et al.')))
	default_conv['Editor'] = (('Format.Editor.Editor.editor4',(' ','','','','')),\
					('Format.Editor.Editor.editor4',(' ','','','','')),\
					('Format.Editor.Editor.editor4',(' ','','','','')),\
					('Format.Editor.Joining.joining1',('',', ',', ',', ',' (Editor)| (Editors)',0,'')))
	def_conv['ARTICLE'] = default_conv
	for typ in BIB_TYPE[1:]:
		def_conv[typ] = def_conv['ARTICLE']
#
class Converter(dict):
	"""Converter Class.
	Converter['ARTICLE']['Author'] = ((AuthorFirst,AuthorFirstParam),(AuthorMiddle,AuthorMiddleParam),(AuthorLast,AuthorLastParam),(AuthorJoin,AuthorJoiningParam))
	etc...
	"""
	def __init__(self,conv=def_conv):
		self.module={}		# dictionary of the formating modules used]
		for typ in BIB_TYPE:
			self[typ] = {}
		"""Import the needed modules and set the converter"""
		for typ in BIB_TYPE:
			for field in BIB_FIELDS:
				for mod in conv[typ][field]:
					if mod[0] and mod[0] not in self.module.keys():	# not None and not already loaded
						#print "Importing %s"%mod[0]
						self.module[mod[0]] = self.__myImport(mod[0])	# we import all the modules
		#
		#
		# conversion dictionary self[BIB_TYPE][BIB_FIELDS] = conversion function
		#
		for k in ('version','locale'):
			self[k] = conv[k]
		#
		for typ in BIB_TYPE:
			for field in BIB_FIELDS:
				if not conv[typ][field][0][0]:	# mod = ((None,any),any)
					self[typ][field] = self.__NullConv,()	# no conversion
				else:
					param = conv[typ][field]	# function parameters
					#func = eval('Converter._Converter__%s' %field)	# name of the formating function. __Author ; __Pages ;
					func = getattr(self,field)
					self[typ][field] = func,param

	def __NullConv(self,s,*p):
		return s

	def __myImport(self,s):
		"""Import the modules"""
		m = __import__(s)
		for i in s.split(".")[1:]:
			m = getattr(m, i)
		return m

	#
	# Define Author conversion
	def Author(self,s,uselocale,*param):
		#print "Author",s,param
		strl = s.split(SEP)
		l = len(strl)
		if l >= 3:
			first,middle,last = strl[0],strl[1:-1],strl[-1]
		elif l == 2:
			first,middle,last = strl[0],'',strl[-1]
		elif l == 1:
			first,middle,last = strl[0],'',''
		else:
			first,middle,last = '','',''
		# Name formating
		#print typ,field
		#print self[typ][field]
		AuthorFirst,AuthorFirstParam = param[0]
		AuthorMiddle,AuthorMiddleParam = param[1]
		AuthorLast,AuthorLastParam = param[2]
		AuthorJoin,AuthorJoiningParam = param[3]
		#
		firstF = apply(self.module[AuthorFirst].format,(first,uselocale)+AuthorFirstParam)
		middleF=[]
		for aut in middle:
			middleF.append(apply(self.module[AuthorMiddle].format,(aut,uselocale)+AuthorMiddleParam))
		lastF = apply(self.module[AuthorLast].format,(last,uselocale)+AuthorLastParam)
		# Name joining
		return apply(self.module[AuthorJoin].format,(firstF,middleF,lastF,uselocale)+AuthorJoiningParam)
	#
	Editor = Author
	#
	def Title(self,s,uselocale,param):
		#print "Title",s,f
		return apply(self.module[param[0]].format,(s,uselocale))
	#
	Booktitle = Title
	#
	def Year(self,s,uselocale,param):
		#print "Year",s,f
		return apply(self.module[param[0]].format,(s,uselocale))
	#
	def Month(self,s,uselocale,param):
		#print "Month",s,f
		return apply(self.module[param[0]].format,(s,uselocale))
	#
	def Pages(self,s,uselocale,param):
		#print "Pages",s,f
		return apply(self.module[param[0]].format,(s,uselocale)+ param[1])
	#
	def Journal(self,s,uselocale,param):
		return apply(self.module[param[0]].format,(s,uselocale))

