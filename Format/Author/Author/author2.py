# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
def format(s,uselocale,*sep):
	"""str = Name, Firstname. sep1 = separator between name and firstname. sep2 = at the end
	out = Pierre<sep1>Emile<sep1>Ulysse<sep2>Martineau"""
	try:
		name,firstname = s.split(',')
		firstname = sep[1].join(firstname.split())
	except ValueError:
		name,firstname = s,''
	name = name.strip()
	firstname = firstname.strip()
	if name != '' and firstname != '':
		return u"%s%s%s%s" %(name,sep[0],firstname,sep[2])
	elif (name,firstname) == ('',''):
		return ''
	elif firstname == '':
		return name
	else:	# name =''
		return u"%s%s" %(firstname,sep[2])

