# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
import wx
#wx.ToolBar,wx.Choice,wx.TextCtrl,wx.Button,wx.EVT_TEXT_ENTER,wx.TE_PROCESS_ENTER,wx.TE_LEFT,wx.EVT_BUTTON,wx.SIZE_USE_EXISTING
import Search,SearchMedline

class myToolBar(wx.Panel):
	def __init__(self,*args, **kwds):
		wx.Panel.__init__(self,*args, **kwds)
		self.parent=self.GetParent()
		self.choice1 = wx.Choice(self,-1,choices=[_("Search"),_("Expert Search"),_("Search PubMed")])
		self.choice1.SetSelection(0)
		self.searchtxt1 = wx.TextCtrl(self,-1,style=wx.TE_PROCESS_ENTER|wx.TE_LEFT)
		self.showall = wx.Button(self,-1,_("Show All"))
		self.showall.Enable(False)
		#
		s0 = wx.BoxSizer(wx.HORIZONTAL)
		s0.Add(self.choice1,0,wx.EXPAND)
		s0.Add(self.searchtxt1,1,wx.ALIGN_CENTER_VERTICAL)
		s0.Add(self.showall,0,wx.ALIGN_CENTER_VERTICAL)
		#
		self.SetSizerAndFit(s0)
		self.Layout()
		self.__set_evt()

	def __set_evt(self):
		wx.EVT_TEXT_ENTER(self,self.searchtxt1.GetId(),self.onSearch)
		wx.EVT_BUTTON(self,self.showall.GetId(),self.onShowAll)

	def __search(self,event,searchstr):
		search = Search.Search(self.parent,self.parent,-1,u'')
		search.SearchValue[0].SetValue(searchstr)
		search.onSearchClose(event)
		self.showall.Enable(True)

	def __searchPubMed(self,event,searchstr):
		search = SearchMedline.SearchMedline(self.parent.db,self.parent,-1,u'')
		search.SearchValue[0].SetValue(searchstr)
		search.onSearchClose(event)

	def __query(self,event,searchstr):
		search = Search.Search(self.parent,self.parent,-1,u'')
		search.Mode.SetStringSelection(_('Expert'))
		search.ExpertValue.SetValue(searchstr)
		search.onSearchClose(event)
		self.showall.Enable(True)

	def onSearch(self,event):
		#print event.GetSelection()
		if self.choice1.GetSelection() ==  0:	# choice search
			self.__search(event,self.searchtxt1.GetValue())
		elif self.choice1.GetSelection() ==  2:	# choice PubMed
			self.__searchPubMed(event,self.searchtxt1.GetValue())
		else:					# choice query = expert mode
			self.__query(event,self.searchtxt1.GetValue())

	def displaySearch(self,searchstr):
		# put searchstr in the search field in Expert Mode and enable showall button
		self.searchtxt1.SetValue(searchstr)
		self.choice1.SetSelection(1)
		self.showall.Enable(True)

	def GetClientWidth(self):
		if wx.Platform == '__WXGTK__':	# bug in wx.Python gtk
			return self.GetSize().GetWidth()-10
		else:
			return self.GetClientSize().GetWidth()

	def onShowAll(self,event):
		self.clearSearch()
		self.onSearch(event)
		self.showall.Enable(False)

	def clearSearch(self):
		self.searchtxt1.SetValue(u'')
		self.showall.Enable(False)

	def withoutPubMed(self):
		"""Remove the search PubMed choice"""
		if self.choice1.GetCount() == 3:
			self.choice1.Delete(2)
			self.choice1.SetSelection(self.choice1.GetSelection())	# needed to reset the choice under GTK

	def withPubMed(self):
		"""Add the search PubMed choice"""
		if self.choice1.GetCount() == 2:
			self.choice1.Append( _("Search PubMed") )

