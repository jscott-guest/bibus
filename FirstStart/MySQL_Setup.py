#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-
# generated by wxGlade 0.4cvs on Sat Apr  8 19:34:49 2006

#import sys

#if not hasattr(sys, 'frozen'):            # we check if we are not freezed using cx_freeze since wxversion
#    try:                                # does not work with freezed scripts
#        import wxversion                # multiversion install
#        wxversion.select( ('2.6','2.5','2.4') )
#    except ImportError:
#        pass

import wx
import MySQLdb
from BIB import DB_TABLE_REF,DB_TABLE_KEY,DB_TABLE_LINK,DB_TABLE_QUERY,DB_TABLE_MODIF,DB_TABLE_FILE

# global variables
db = None    # connection to MySQL
dbCursor = None                # cursor for current db
db_name = ""             # name of the current database
tableref = DB_TABLE_REF
tablekey = DB_TABLE_KEY
tablelink = DB_TABLE_LINK
tablequery = DB_TABLE_QUERY
tablemodif = DB_TABLE_MODIF
tablefile = DB_TABLE_FILE

order_grands = ('rw','ro','rk','rr')  # grants order in choices

def setGrants(user,grants):
    # defining grants for each user type
    if grants == 'rw':
        dbCursor.execute("GRANT  SELECT, INSERT, UPDATE, DELETE, CREATE TEMPORARY TABLES ON `%s`.* TO %s" %(db_name,user))
    elif grants == 'ro':
        dbCursor.execute("GRANT SELECT ON `%s`.`%s` TO %s" %(db_name,tableref,user))
        dbCursor.execute("GRANT SELECT, INSERT, UPDATE, DELETE ON `%s`.`%s` TO %s" %(db_name,tablekey,user))
        dbCursor.execute("GRANT SELECT, INSERT, UPDATE, DELETE ON `%s`.`%s` TO %s" %(db_name,tablequery,user))
        dbCursor.execute("GRANT SELECT, INSERT, UPDATE, DELETE ON `%s`.`%s` TO %s" %(db_name,tablelink,user))
        dbCursor.execute("GRANT SELECT ON `%s`.`%s` TO %s" %(db_name,tablemodif,user))
        dbCursor.execute("GRANT SELECT ON `%s`.`%s` TO %s" %(db_name,tablefile,user))
    elif grants == 'rk':
        dbCursor.execute("GRANT SELECT ON `%s`.* TO %s" %(db_name,user))
    else:
        dbCursor.execute("GRANT SELECT ON `%s`.`%s` TO %s" %(db_name,tableref,user))
    db.commit()
#
def clearGrants(user):
#    dbCursor.execute("REVOKE ALL PRIVILEGES ON `%s`.`%s` FROM %s"%(db_name,tableref,user))
#    dbCursor.execute("REVOKE ALL PRIVILEGES ON `%s`.`%s` FROM %s"%(db_name,tablekey,user))
#    dbCursor.execute("REVOKE ALL PRIVILEGES ON `%s`.`%s` FROM %s"%(db_name,tablequery,user))
#    dbCursor.execute("REVOKE ALL PRIVILEGES ON `%s`.`%s` FROM %s"%(db_name,tablelink,user))
#    dbCursor.execute("REVOKE ALL PRIVILEGES ON `%s`.* FROM %s"%(db_name,user))
    username,host = user.split('@')
    dbCursor.execute("DELETE FROM mysql.db where user=%s and host=%s and db='%s'"%(username,host,db_name))
    for table in (tableref,tablekey,tablequery,tablelink):
        dbCursor.execute("DELETE FROM mysql.tables_priv where user=%s and host=%s and db='%s' and table_name='%s'"%(username,host,db_name,table))
    dbCursor.execute("FLUSH PRIVILEGES")
    db.commit()

class ModifyUser(wx.Dialog):
    def __init__(self, *args, **kwds):
        # begin wxGlade: ModifyUser.__init__
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, *args, **kwds)
        self.label_8 = wx.StaticText(self, -1, _("User Name"))
        self.choice_user = wx.Choice(self, -1, choices=[])
        self.label_9 = wx.StaticText(self, -1, _("Grants"))
        self.choice_grants = wx.Choice(self, -1, choices=[_("Normal (rw)"), _("Read-only with private and shared trees (ro)"), _("Read-only with shared tree only (rk)"), _("Read-only without tree (rr)")])
        self.button_cancel = wx.Button(self, wx.ID_CANCEL, _("Cancel"))
        self.button_ok = wx.Button(self, wx.ID_OK, _("OK"))

        self.__set_properties()
        self.__do_layout()

        wx.EVT_BUTTON(self, wx.ID_OK, self.onOK)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: ModifyUser.__set_properties
        self.SetTitle(_("Modify User Grants"))
        self.choice_grants.SetSelection(0)
        self.button_ok.SetDefault()
        # end wxGlade
        dbCursor.execute("select User,Host from mysql.user")
        for user,host in dbCursor.fetchall():
            self.choice_user.Append("'%s'@'%s'" %(user,host))
        self.choice_user.SetSelection(0)

    def __do_layout(self):
        # begin wxGlade: ModifyUser.__do_layout
        sizer_13 = wx.BoxSizer(wx.VERTICAL)
        sizer_12_copy = wx.BoxSizer(wx.HORIZONTAL)
        grid_sizer_4 = wx.FlexGridSizer(2, 2, 3, 3)
        grid_sizer_4.Add(self.label_8, 0, wx.LEFT|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 5)
        grid_sizer_4.Add(self.choice_user, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_4.Add(self.label_9, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
        grid_sizer_4.Add(self.choice_grants, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_4.AddGrowableCol(1)
        sizer_13.Add(grid_sizer_4, 1, wx.EXPAND, 0)
        sizer_12_copy.Add(self.button_cancel, 0, wx.ALL|wx.ADJUST_MINSIZE, 5)
        sizer_12_copy.Add(self.button_ok, 0, wx.ALL|wx.ADJUST_MINSIZE, 5)
        sizer_13.Add(sizer_12_copy, 0, wx.TOP|wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.SetAutoLayout(True)
        self.SetSizer(sizer_13)
        sizer_13.Fit(self)
        sizer_13.SetSizeHints(self)
        self.Layout()
        self.Centre()
        # end wxGlade

    def onOK(self, event): # wxGlade: ModifyUser.<event_handler>
        username = self.choice_user.GetStringSelection()
        clearGrants(username)
        setGrants(username,order_grands[self.choice_grants.GetSelection()])
        self.EndModal(wx.ID_OK)
        self.Destroy()

# end of class ModifyUser


class MySQL_Setup_Main(wx.Dialog):
    def __init__(self, *args, **kwds):
        # begin wxGlade: MySQL_Setup_Main.__init__
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, *args, **kwds)
        self.connect = wx.Button(self, -1, _("Connect..."))
        self.connect_status = wx.TextCtrl(self, -1, _("Not connected"), style=wx.TE_READONLY)
        self.database = wx.Button(self, -1, _("Database..."))
        self.database_status = wx.TextCtrl(self, -1, _("No Database selected"), style=wx.TE_READONLY)
        self.create_user = wx.Button(self, -1, _("Create user..."))
        self.create_user_status = wx.TextCtrl(self, -1, "", style=wx.TE_READONLY)
        self.modify_user = wx.Button(self, -1, _("Modify user..."))
        self.modify_user_status = wx.TextCtrl(self, -1, "", style=wx.TE_READONLY)
        self.quit = wx.Button(self, -1, _("Quit"))

        self.__set_properties()
        self.__do_layout()

        wx.EVT_BUTTON(self, self.connect.GetId(), self.onConnect)
        wx.EVT_BUTTON(self, self.database.GetId(), self.onDatabase)
        wx.EVT_BUTTON(self, self.create_user.GetId(), self.onCreateUser)
        wx.EVT_BUTTON(self, self.modify_user.GetId(), self.onModifyUser)
        wx.EVT_BUTTON(self, self.quit.GetId(), self.onQuit)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: MySQL_Setup_Main.__set_properties
        self.SetTitle(_("Bibus MySQL Setup"))
        self.SetSize((495, 180))
        # end wxGlade
        self.enableButtons()

    def enableButtons(self):
        self.database.Disable()
        self.create_user.Disable()
        self.modify_user.Disable()
        if db:
            self.database.Enable()
            if db_name:
                self.create_user.Enable()
                self.modify_user.Enable()

    def __do_layout(self):
        # begin wxGlade: MySQL_Setup_Main.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_1 = wx.FlexGridSizer(4, 2, 3, 3)
        grid_sizer_1.Add(self.connect, 0, wx.ADJUST_MINSIZE, 0)
        grid_sizer_1.Add(self.connect_status, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_1.Add(self.database, 0, wx.ADJUST_MINSIZE, 0)
        grid_sizer_1.Add(self.database_status, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_1.Add(self.create_user, 0, wx.ADJUST_MINSIZE, 0)
        grid_sizer_1.Add(self.create_user_status, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_1.Add(self.modify_user, 0, wx.ADJUST_MINSIZE, 0)
        grid_sizer_1.Add(self.modify_user_status, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_1.AddGrowableCol(1)
        sizer_1.Add(grid_sizer_1, 0, wx.EXPAND, 0)
        sizer_1.Add(self.quit, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.ADJUST_MINSIZE, 10)
        self.SetAutoLayout(True)
        self.SetSizer(sizer_1)
        self.Layout()
        self.Centre()
        # end wxGlade

    def onConnect(self, event): # wxGlade: MySQL_Setup_Main.<event_handler>
        dlg = Connect(self)
        try:
            answer = dlg.ShowModal()
        finally:
            dlg.Destroy()
        if answer == wx.ID_OK:
            self.connect_status.SetValue("Connected to MySQL")
        else:
            self.connect_status.SetValue("Not connected")
        self.enableButtons()

    def onDatabase(self, event): # wxGlade: MySQL_Setup_Main.<event_handler>
        dlg = Db(self)
        try:
            answer = dlg.ShowModal()
        finally:
            dlg.Destroy()
        if db_name:
            self.database_status.SetValue("Database selected = %s"%db_name)
        else:
            self.database_status.SetValue("No Database selected")
        self.enableButtons()

    def onCreateUser(self, event): # wxGlade: MySQL_Setup_Main.<event_handler>
        dlg = CreateUser(self)
        try:
            answer = dlg.ShowModal()
        finally:
            dlg.Destroy()
        if answer == wx.ID_OK:
            self.create_user_status.SetValue("User created")
        else:
            self.create_user_status.SetValue("No user created")

    def onModifyUser(self, event): # wxGlade: MySQL_Setup_Main.<event_handler>
        dlg = ModifyUser(self)
        try:
            answer = dlg.ShowModal()
        finally:
            dlg.Destroy()
        if answer == wx.ID_OK:
            self.modify_user_status.SetValue("User modified")
        else:
            self.modify_user_status.SetValue("Operation cancelled")

    def onQuit(self, event): # wxGlade: MySQL_Setup_Main.<event_handler>
        self.Destroy()

# end of class MySQL_Setup_Main


class Connect(wx.Dialog):
    def __init__(self, *args, **kwds):
        # begin wxGlade: Connect.__init__
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, *args, **kwds)
        self.label_1 = wx.StaticText(self, -1, _("User"))
        self.text_ctrl_user = wx.TextCtrl(self, -1, _("root"))
        self.label_2 = wx.StaticText(self, -1, _("Password"))
        self.text_ctrl_passwd = wx.TextCtrl(self, -1, "", style=wx.TE_PASSWORD)
        self.label_3 = wx.StaticText(self, -1, _("Host"))
        self.text_ctrl_host = wx.TextCtrl(self, -1, _("localhost"))
        self.label_4 = wx.StaticText(self, -1, _("Port"))
        self.text_ctrl_port = wx.TextCtrl(self, -1, _("3306"))
        self.label_5 = wx.StaticText(self, -1, _("Socket"))
        self.text_ctrl_socket = wx.TextCtrl(self, -1, "/var/run/mysqld/mysqld.sock")
        self.button_cancel = wx.Button(self, wx.ID_CANCEL, _("Cancel"))
        self.button_connect = wx.Button(self, wx.ID_OK, _("Connect"))

        self.__set_properties()
        self.__do_layout()

        wx.EVT_BUTTON(self, wx.ID_OK, self.onConnect)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: Connect.__set_properties
        self.SetTitle(_("Connection"))
        self.SetSize((350, 182))
        self.button_connect.SetDefault()
        # end wxGlade
        self.text_ctrl_passwd.SetFocus()

    def __do_layout(self):
        # begin wxGlade: Connect.__do_layout
        sizer_2 = wx.BoxSizer(wx.VERTICAL)
        sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        grid_sizer_2 = wx.FlexGridSizer(5, 2, 3, 3)
        grid_sizer_2.Add(self.label_1, 0, wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
        grid_sizer_2.Add(self.text_ctrl_user, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_2.Add(self.label_2, 0, wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
        grid_sizer_2.Add(self.text_ctrl_passwd, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_2.Add(self.label_3, 0, wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
        grid_sizer_2.Add(self.text_ctrl_host, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_2.Add(self.label_4, 0, wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
        grid_sizer_2.Add(self.text_ctrl_port, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_2.Add(self.label_5, 0, wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
        grid_sizer_2.Add(self.text_ctrl_socket, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_2.AddGrowableCol(1)
        sizer_2.Add(grid_sizer_2, 1, wx.EXPAND, 0)
        sizer_3.Add(self.button_cancel, 0, wx.ALL|wx.ADJUST_MINSIZE, 5)
        sizer_3.Add(self.button_connect, 0, wx.ALL|wx.ADJUST_MINSIZE, 5)
        sizer_2.Add(sizer_3, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        self.SetAutoLayout(True)
        self.SetSizer(sizer_2)
        self.Layout()
        self.Centre()
        # end wxGlade

    def onConnect(self, event): # wxGlade: Connect.<event_handler>
        global db,dbCursor
        db = MySQLdb.connect(user=self.text_ctrl_user.GetValue(),
                            passwd=self.text_ctrl_passwd.GetValue(),
                            host=self.text_ctrl_host.GetValue(),
                            port=int(self.text_ctrl_port.GetValue()),
                            unix_socket=self.text_ctrl_socket.GetValue())
        dbCursor = db.cursor()
        self.EndModal(wx.ID_OK)
        self.Destroy()

# end of class Connect

class CreateDb(wx.Dialog):
    def __init__(self, *args, **kwds):
        # begin wxGlade: CreateDb.__init__
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, *args, **kwds)
        self.radio_box_dbType = wx.RadioBox(self, -1, _("Database Type"), choices=[_("MySQL v3 and v4"), _("MySQL v4.1 and v5")], majorDimension=0, style=wx.RA_SPECIFY_ROWS)
        self.label_6 = wx.StaticText(self, -1, _("Database Name"))
        self.text_ctrl_dbName = wx.TextCtrl(self, -1, _("biblioDB"))
        self.button_cancel = wx.Button(self, wx.ID_CANCEL, _("Cancel"))
        self.button_ok = wx.Button(self, wx.ID_OK, _("OK"))

        self.__set_properties()
        self.__do_layout()

        wx.EVT_BUTTON(self, wx.ID_OK, self.onOK)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: CreateDb.__set_properties
        self.SetTitle(_("Database Creation"))
        self.radio_box_dbType.SetSelection(0)
        self.button_ok.SetDefault()
        # end wxGlade
        dbCursor.execute("SELECT VERSION()")
        mysqlversion =dbCursor.fetchone()[0].split('.')[:2]
        if mysqlversion >= ['4','1']:
            self.radio_box_dbType.SetSelection(1)
        else:
            self.radio_box_dbType.SetSelection(0)

    def __do_layout(self):
        # begin wxGlade: CreateDb.__do_layout
        sizer_6 = wx.BoxSizer(wx.VERTICAL)
        sizer_8 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_7 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_6.Add(self.radio_box_dbType, 0, wx.ALL|wx.ADJUST_MINSIZE, 3)
        sizer_7.Add(self.label_6, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 3)
        sizer_7.Add(self.text_ctrl_dbName, 1, wx.ADJUST_MINSIZE, 0)
        sizer_6.Add(sizer_7, 0, wx.EXPAND, 0)
        sizer_8.Add(self.button_cancel, 0, wx.ALL|wx.ADJUST_MINSIZE, 5)
        sizer_8.Add(self.button_ok, 0, wx.ALL|wx.ADJUST_MINSIZE, 5)
        sizer_6.Add(sizer_8, 0, wx.EXPAND, 0)
        self.SetAutoLayout(True)
        self.SetSizer(sizer_6)
        sizer_6.Fit(self)
        sizer_6.SetSizeHints(self)
        self.Layout()
        self.Centre()
        # end wxGlade

    def onOK(self, event): # wxGlade: CreateDb.<event_handler>
        dbCursor.execute("SHOW databases")
        newdbname = self.text_ctrl_dbName.GetValue()
        if newdbname in [d[0] for d in dbCursor.fetchall()]:
            ret = wx.MessageBox("Database %s already exists. If you say OK below, all the data will be lost. Do you really want to drop the database %s?"%(newdbname,newdbname),"Database exists",style=wx.OK|wx.CANCEL|wx.ICON_EXCLAMATION)
            if ret == wx.OK:
                dbCursor.execute("DROP DATABASE %s"%newdbname)
            else:
                self.Destroy()
                return
        if self.radio_box_dbType.GetSelection() == 0:
            from mysql_tables import TABLE_KEY, TABLE_LINK, TABLE_QUERY, TABLE_REF, TABLE_MODIF, TABLE_FILE
        else:
            from mysql_41_tables import TABLE_KEY, TABLE_LINK, TABLE_QUERY, TABLE_REF, TABLE_MODIF, TABLE_FILE
        dbCursor.execute("CREATE DATABASE %s"%newdbname)
        dbCursor.execute("USE %s"%newdbname)
        dbCursor.execute("""create table %s %s""" % (tableref,TABLE_REF))
        dbCursor.execute("""create table %s %s""" % (tablekey,TABLE_KEY))
        dbCursor.execute("""create table %s %s""" % (tablelink,TABLE_LINK))
        dbCursor.execute("""create table %s %s""" % (tablequery,TABLE_QUERY))
        dbCursor.execute("""create table %s %s""" % (tablemodif,TABLE_MODIF))
        dbCursor.execute("""create table %s %s""" % (tablefile,TABLE_FILE))
        db.commit()
        self.EndModal(wx.ID_OK)
        self.Destroy()

# end of class CreateDb

class Db(wx.Dialog):
    def __init__(self, *args, **kwds):
        # begin wxGlade: Db.__init__
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, *args, **kwds)
        self.label_7 = wx.StaticText(self, -1, _("Use Database:"))
        self.choice_db = wx.Choice(self, -1, choices=[])
        self.button_createDB = wx.Button(self, -1, _("New Database..."))
        self.button_cancel = wx.Button(self, wx.ID_CANCEL, _("Cancel"))
        self.button_ok = wx.Button(self, wx.ID_OK, _("OK"))

        self.__set_properties()
        self.__do_layout()

        wx.EVT_BUTTON(self, self.button_createDB.GetId(), self.onNewDatabase)
        wx.EVT_BUTTON(self, wx.ID_OK, self.onOK)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: Db.__set_properties
        self.SetTitle(_("Database"))
        self.button_ok.SetDefault()
        # end wxGlade
        self.displayDB()

    def displayDB(self):
        self.choice_db.Clear()
        dbCursor.execute("SHOW databases")
        for d in dbCursor.fetchall():
            self.choice_db.Append(d[0])
        if db_name:
            self.choice_db.SetStringSelection(db_name)
        else:
            self.choice_db.SetSelection(0)

    def __do_layout(self):
        # begin wxGlade: Db.__do_layout
        sizer_4 = wx.BoxSizer(wx.VERTICAL)
        sizer_5 = wx.BoxSizer(wx.HORIZONTAL)
        grid_sizer_3 = wx.FlexGridSizer(2, 2, 3, 3)
        grid_sizer_3.Add(self.label_7, 0, wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
        grid_sizer_3.Add(self.choice_db, 0, wx.ADJUST_MINSIZE, 30)
        grid_sizer_3.Add((20, 20), 0, wx.ADJUST_MINSIZE, 0)
        grid_sizer_3.Add(self.button_createDB, 0, wx.ADJUST_MINSIZE, 0)
        grid_sizer_3.AddGrowableCol(1)
        sizer_4.Add(grid_sizer_3, 1, wx.EXPAND, 0)
        sizer_5.Add(self.button_cancel, 0, wx.ALL|wx.ADJUST_MINSIZE, 3)
        sizer_5.Add(self.button_ok, 0, wx.ALL|wx.ADJUST_MINSIZE, 3)
        sizer_4.Add(sizer_5, 0, wx.TOP|wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.SetAutoLayout(True)
        self.SetSizer(sizer_4)
        sizer_4.Fit(self)
        sizer_4.SetSizeHints(self)
        self.Layout()
        self.Centre()
        # end wxGlade

    def onNewDatabase(self, event): # wxGlade: Db.<event_handler>
        dlg = CreateDb(self)
        try:
            answer = dlg.ShowModal()
        finally:
            dlg.Destroy()
        if answer == wx.ID_OK:
            self.displayDB()

    def onOK(self, event): # wxGlade: Db.<event_handler>
        global db_name
        db_name = self.choice_db.GetStringSelection()
        self.EndModal(wx.ID_OK)
        self.Destroy()

# end of class Db


class CreateUser(wx.Dialog):
    def __init__(self, *args, **kwds):
        # begin wxGlade: CreateUser.__init__
        kwds["style"] = wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, *args, **kwds)
        self.label_8_copy_1 = wx.StaticText(self, -1, _("User Name"))
        self.text_ctrl_username = wx.TextCtrl(self, -1, "")
        self.label_11 = wx.StaticText(self, -1, _("Host"))
        self.text_ctrl_host = wx.TextCtrl(self, -1, _("%"))
        self.label_10 = wx.StaticText(self, -1, _("Password"))
        self.text_ctrl_passwd = wx.TextCtrl(self, -1, "", style=wx.TE_PASSWORD)
        self.label_9_copy = wx.StaticText(self, -1, _("Grants"))
        self.choice_grants = wx.Choice(self, -1, choices=[_("Normal (rw)"), _("Read-only with private and shared trees (ro)"), _("Read-only with shared tree only (rk)"), _("Read-only without tree (rr)")])
        self.button_cancel = wx.Button(self, wx.ID_CANCEL, _("Cancel"))
        self.button_createUser = wx.Button(self, wx.ID_OK, _("Create User"))

        self.__set_properties()
        self.__do_layout()

        wx.EVT_BUTTON(self, wx.ID_OK, self.onCreateUser)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: CreateUser.__set_properties
        self.SetTitle(_("User Creation"))
        self.choice_grants.SetSelection(0)
        self.button_createUser.SetDefault()
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: CreateUser.__do_layout
        sizer_9 = wx.BoxSizer(wx.VERTICAL)
        sizer_12 = wx.BoxSizer(wx.HORIZONTAL)
        grid_sizer_5 = wx.FlexGridSizer(5, 2, 3, 3)
        grid_sizer_5.Add(self.label_8_copy_1, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 5)
        grid_sizer_5.Add(self.text_ctrl_username, 1, wx.EXPAND|wx.ADJUST_MINSIZE, 5)
        grid_sizer_5.Add(self.label_11, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
        grid_sizer_5.Add(self.text_ctrl_host, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_5.Add(self.label_10, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
        grid_sizer_5.Add(self.text_ctrl_passwd, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_5.Add(self.label_9_copy, 0, wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL|wx.ADJUST_MINSIZE, 0)
        grid_sizer_5.Add(self.choice_grants, 0, wx.EXPAND|wx.ADJUST_MINSIZE, 0)
        grid_sizer_5.AddGrowableCol(1)
        sizer_9.Add(grid_sizer_5, 1, wx.ALL|wx.EXPAND, 5)
        sizer_12.Add(self.button_cancel, 0, wx.ALL|wx.ADJUST_MINSIZE, 5)
        sizer_12.Add(self.button_createUser, 0, wx.ALL|wx.ADJUST_MINSIZE, 5)
        sizer_9.Add(sizer_12, 0, wx.TOP|wx.ALIGN_CENTER_HORIZONTAL, 10)
        self.SetAutoLayout(True)
        self.SetSizer(sizer_9)
        sizer_9.Fit(self)
        sizer_9.SetSizeHints(self)
        self.Layout()
        self.Centre()
        # end wxGlade

    def onCreateUser(self, event): # wxGlade: CreateUser.<event_handler>
        username = self.text_ctrl_username.GetValue()
        host = self.text_ctrl_host.GetValue()
        passwd = self.text_ctrl_passwd.GetValue()
        # we check if the user already exists
        dbCursor.execute("select User,Host from mysql.user")
        if (username,host) in dbCursor.fetchall():
            wx.MessageBox("User %s already exists. Please use the button 'Modify User' if you want to set his grants"%"'%s'@'%s'"%(username,host),"User exists",style=wx.OK|wx.ICON_ERROR)
            self.EndModal(wx.ID_CANCEL)
        else:
            dbCursor.execute("GRANT USAGE ON *.* TO '%s'@'%s' IDENTIFIED BY '%s'" %(username,host,passwd))
            setGrants("'%s'@'%s'"%(username,host),order_grands[self.choice_grants.GetSelection()])
            self.EndModal(wx.ID_OK)
        self.Destroy()

# end of class CreateUser


if __name__ == "__main__":
    app = wx.PySimpleApp(0)
    wx.InitAllImageHandlers()
    MySQL_Setup = MySQL_Setup_Main(None, -1, "")
    app.SetTopWindow(MySQL_Setup)
    MySQL_Setup.Show()
    app.MainLoop()
