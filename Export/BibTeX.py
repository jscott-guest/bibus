# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
# EndNote refer format
# from http://www.ecst.csuchico.edu/~jacobsd/bib/formats/endnote.html
#
import BIB

DEFAULT_ENCODING = 'latin_1'

class exportRef(object):
	"""Class is iterable. Return records one by one."""
	# conversion OOo <-> EndNote Publication Type: dictionary Type[OOo Name]=EndNote/refed.
 	Type={
	'ARTICLE':'article',
	'BOOK':'book',
	'BOOKLET':'booklet',
	'CONFERENCE':'conference',
	'INBOOK':'inbook',
	'INCOLLECTION':'incollection',
	'INPROCEEDINGS':'inproceedings',
	'MANUAL':'manual',
	'MASTERSTHESIS':'masterthesis',
	'MISC':'misc',
	'PHDTHESIS':'phdthesis',
	'PROCEEDINGS':'proceedings',
	'TECHREPORT':'techreport',
	'UNPUBLISHED':'unpublished',
	'WWW':'misc' # Map WWW to misc
	}	

	# We are using the export list for simplisity
	Fields={
	'address':3,
	'annote':4,
	'author':5,
	'booktitle':6,
	'chapter':7,
	'edition':8,
	'editor':9,
	'howpublished':10,
	'institution':11,
	'journal':12,
	'month':13,
	'note':14,
	'number':15,
	'organization':16,
	'pages':17,
	'publisher':18,
	'school':19,
	'series':20,
	'title':21,
	'volume':23,
	'year':24,
	'url':25,
	'doi':26,
	'location':27,
	'abstract':32,
	'isbn':31}

	def __init__(self,infile):
		self.infile = infile	# must be a file type. Need a write() function.

	def write(self,ref):
		"""write(ref)"""
		record = self.__convertRecord(ref)
		if record:
			self.infile.write(record)
			self.infile.write('\n\n')		# add a blank line to separate records

	def __convertRecord(self,ref):
		"""Convert a OOo reference to a BibTex format
		input ==
		('Id','Identifier', 'Bibliographic_Type', 'Address', 'Annote', 'Author', 'Booktitle', 'Chapter', 'Edition', 'Editor','HowPublished', 'Institution', 'Journal', 'Month', 'Note', 'Number', 'Organizations', 'Pages', 'Publisher', 'School', 'Series', 'Title', 'Report_Type', 'Volume', 'Year', 'URL', 'Custom1', 'Custom2', 'Custom3', 'Custom4', 'Custom5', 'ISBN','Abstract')"""
		#
		record=[]
		# Type
		try:
			record.append( "@%s{%s,"%(exportRef.Type[BIB.BIB_TYPE[ref[2]]], ref[1]) )
		
			for fname in exportRef.Fields:
				i = exportRef.Fields[fname]
				if ref[i]:
					if fname == 'author':
						record.append( "\t%s = {%s}, "%(fname," and ".join(ref[i].split(";"))))
					else:
						record.append( "\t%s = {%s}, "%(fname,ref[i]))
			record.append("}")
		except:
			print "Not exporting record %s (%s)"%(ref[1],ref[2])
			pass # We should log the error here
		return '\n'.join(record)
		
