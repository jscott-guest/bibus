# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
import BIB

DEFAULT_ENCODING = 'latin_1'

class exportRef(object):
	"""Class is iterable. Return records one by one."""
	# conversion OOo <-> RIS Publication Type: dictionary Type[OOo Name]=RIS.
 	Type={
 	'ARTICLE':'JOUR' ,
 	'BOOK':'BOOK',
	'BOOKLET':'BOOK' ,
	'CONFERENCE':'CONF' ,
	'INBOOK':'CHAP' ,
	'INCOLLECTION':'SER' ,
	'INPROCEEDINGS':'CONF' ,
	'JOURNAL':'JFULL' ,
	'MANUAL':'BOOK' ,
	'MASTERTHESIS':'THES' ,
	'MISC':'GEN' ,
	'PHDTHESIS':'THES' ,
	'PROCEEDINGS':'CONF' ,
	'TECHREPORT':'RPRT' ,
	'UNPUBLISHED':'UNPB' ,
	'EMAIL':'ICOMM' ,
	'WWW':'ICOMM' ,
	'CUSTOM1':'GEN' ,
	'CUSTOM2':'GEN' ,
	'CUSTOM3':'GEN' ,
	'CUSTOM4':'GEN' ,
	'CUSTOM5':'GEN'}

	def __init__(self,infile):
		self.infile = infile	# must be a file type. Need a write() function.

	def write(self,ref):
		"""write(ref)"""
		record = self.__convertRecord(ref)
		if record:
			self.infile.write(record)
			self.infile.write('\nER  - \n')		# add a 'ER  - '

	def __convertRecord(self,ref):
		"""Convert a OOo reference to an endnote record
		input ==
		('Id','Identifier', 'Bibliographic_Type', 'Address', 'Annote', 'Author', 'Booktitle', 'Chapter', 'Edition', 'Editor','HowPublished', 'Institution', 'Journal', 'Month', 'Note', 'Number', 'Organizations', 'Pages', 'Publisher', 'School', 'Series', 'Title', 'Report_Type', 'Volume', 'Year', 'URL', 'Custom1', 'Custom2', 'Custom3', 'Custom4', 'Custom5', 'ISBN','Abstract')"""
		#
		record=[]
		# Type
		record.append( "TY  - %s"%exportRef.Type[BIB.BIB_TYPE[ref[2]]] )
		# Address
		if ref[3]: record.append( "AD  - %s"%ref[3] )
		# Annote
		#if ref[4]:
		# Author
		if ref[5]:
			record.extend( map( lambda x: "A1  - %s"%x , ref[5].split(BIB.SEP) ) )
		# Booktitle
		if ref[6]:
			record.append( "BT  - %s"%ref[6] )
		# Chapter
		# if ref[7]: Don't know where to put the chapter
		# Edition
		#if ref[8]:
		# Editor
		if ref[9]:
			record.extend( map( lambda x: "A3  - %s"%x , ref[9].split(BIB.SEP) ) )
		# HowPublished
		#if ref[10]:
		# Institution
		#if ref[11]:
		# Journal
		try:
			med,iso,full = BIB.JOURNAL[ BIB.JOURNAL_ALTERNATE[ ref[12].upper() ] ]
			record.append( "JA  - %s"%med )		# Abbreviated journal
			record.append( "JO  - %s"%full )	# Full journal
		except:
			record.append( "JA  - %s"%ref[12] )		# Abbreviated journal
		# Year(24) Month(13)
		if ref[24]:
			record.append( "Y1  - %s/%s/"%(ref[24],ref[13]) )
		# Note
		if ref[14]:
			record.append( "N1  - %s"%ref[14] )
		# Number
		if ref[15]:
			record.append( "IS  - %s"%ref[15] )
		# Organizations
		# Pages
		if ref[17]:
			record.append( "SP  - %s"%ref[17].split('-')[0] )
			try:
				record.append( "EP  - %s"%ref[17].split('-')[1] )
			except IndexError:
				pass
		# Publisher
		if ref[18]:
			record.append( "PB  - %s"%ref[18] )
		# School
		# if ref[19]:
		# Series
		if ref[20]:
			record.append( "T3  - %s"%ref[20] )
		# Title
		if ref[21]:
			record.append( "T1  - %s"%ref[21] )
		# Report_Type
		#if ref[22]:
		# Volume
		if ref[23]:
			record.append( "VL  - %s"%ref[23] )
		# Year
		# see Month
		# URL
		if ref[25]:
			record.append( "UR  - %s"%ref[25] )
		# Custom1
		if ref[26]:
			record.append( "U1  - %s"%ref[26] )
		# Custom1
		if ref[27]:
			record.append( "U2  - %s"%ref[27] )
		# Custom1
		if ref[28]:
			record.append( "U3  - %s"%ref[28] )
		# Custom1
		if ref[29]:
			record.append( "U4  - %s"%ref[29] )
		# Custom1
		if ref[30]:
			record.append( "U5  - %s"%ref[30] )
		# ISBN
		if ref[31]:
			record.append( "SN  - %s"%ref[30] )
		# Abstract
		if ref[32]:
			record.append( "N2  - %s"%ref[32] )
		#
		return '\n'.join(record)
